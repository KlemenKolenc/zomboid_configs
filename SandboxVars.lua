SandboxVars = {
    VERSION = 4,
    Zombies = 3,
    Distribution = 1,
    DayLength = 3,
    StartYear = 1,
    StartMonth = 7,
    StartDay = 9,
    StartTime = 2,
    WaterShut = 7,
    ElecShut = 3,
    WaterShutModifier = 21,
    ElecShutModifier = 21,
    FoodLoot = 4,
    WeaponLoot = 2,
    OtherLoot = 3,
    Temperature = 2,
    Rain = 3,
    ErosionSpeed = 3,
    ErosionDays = 3,
    XpMultiplier = 1.0,
    ZombieAttractionMultiplier = 1.0,
    VehicleEasyUse = false,
    Farming = 3,
    CompostTime = 2,
    StatsDecrease = 4,
    NatureAbundance = 3,
    Alarm = 3,
    LockedHouses = 5,
    StarterKit = false,
    Nutrition = true,
    FoodRotSpeed = 3,
    FridgeFactor = 3,
    LootRespawn = 5,
    SeenHoursPreventLootRespawn = 0,
    TimeSinceApo = 1,
    PlantResilience = 3,
    PlantAbundance = 4,
    EndRegen = 3,
    Helicopter = 3,
    MetaEvent = 2,
    SleepingEvent = 2,
    GeneratorSpawning = 1,
    GeneratorFuelConsumption = 0.8,
    SurvivorHouseChance = 2,
    AnnotatedMapChance = 3,
    CharacterFreePoints = 2,
    ConstructionBonusPoints = 3,
    NightDarkness = 2,
    InjurySeverity = 1,
    BoneFracture = true,
    HoursForCorpseRemoval = 24,
    DecayingCorpseHealthImpact = 3,
    BloodLevel = 5,
    ClothingDegradation = 2,
    FireSpread = true,
    DaysForRottenFoodRemoval = 24,
    AllowExteriorGenerator = true,
    MaxFogIntensity = 1,
    MaxRainFxIntensity = 1,
    EnableSnowOnGround = true,
    CarSpawnRate = 3,
    ChanceHasGas = 2,
    InitialGas = 2,
    CarGasConsumption = 1.0,
    LockedCar = 5,
    CarGeneralCondition = 3,
    CarDamageOnImpact = 3,
    DamageToPlayerFromHitByACar = 1,
    TrafficJam = true,
    CarAlarm = 2,
    PlayerDamageFromCrash = true,
    SirenShutoffHours = 1.0,
    RecentlySurvivorVehicles = 1,
    EnableVehicles = true,
    ZombieLore = {
        Speed = 2,
        Strength = 3,
        Toughness = 2,
        Transmission = 1,
        Mortality = 5,
        Reanimate = 4,
        Cognition = 2,
        Memory = 2,
        Decomp = 1,
        Sight = 2,
        Hearing = 2,
        Smell = 2,
        ThumpNoChasing = false,
        ThumpOnConstruction = true,
        ActiveOnly = 1,
        TriggerHouseAlarm = false,
    },
    ZombieConfig = {
        PopulationMultiplier = 2.0,
        PopulationStartMultiplier = 1.0,
        PopulationPeakMultiplier = 3.0,
        PopulationPeakDay = 90,
        RespawnHours = 48.0,
        RespawnUnseenHours = 24.0,
        RespawnMultiplier = 0.1,
        RedistributeHours = 12.0,
        FollowSoundDistance = 100,
        RallyGroupSize = 20,
        RallyTravelDistance = 20,
        RallyGroupSeparation = 15,
        RallyGroupRadius = 3,
    },
}
